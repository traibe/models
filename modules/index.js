const {
    mongoose,
    DatabaseConnection
} = require('./database');

module.exports = {
    mongoose,
    DatabaseConnection
}