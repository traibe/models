var validate = require('mongoose-validator')

const urlValidator = validate({
    validator           : 'isURL',
    message             : 'Must be a Valid URL',
    protocols           : ['http','https','ftp'], 
    require_tld         : true, 
    require_protocol    : true 
});

module.exports = {
    urlValidator
}