
const forceDefault = function(next) {
    const self = this;
    Object.keys(this.schema.paths).forEach(function(key) {
        let spk = self.schema.paths[key];
        if(spk.options.default && self[key] === null){
            self[key] = spk.options.default;
            self.markModified(key); // In case of mixed
        }
    });
    next();
};

module.exports = {forceDefault}