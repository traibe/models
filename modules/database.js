/**
    ------------------------------------------------------------------------------
    
    ooooooooooooo ooooooooo.         .o.       ooooo oooooooooo.  oooooooooooo 
    8'   888   `8 `888   `Y88.      .888.      `888' `888'   `Y8b `888'     `8 
         888       888   .d88'     .8"888.      888   888     888  888         
         888       888ooo88P'     .8' `888.     888   888oooo888'  888oooo8    
         888       888`88b.      .88ooo8888.    888   888    `88b  888    "    
         888       888  `88b.   .8'     `888.   888   888    .88P  888       o 
        o888o     o888o  o888o o88o     o8888o o888o o888bood8P'  o888ooooood8    

    ------------------------------------------------------------------------------
    Database Configuration Class
    ------------------------------------------------------------------------------

    Author: Nicholas Hamilton, PhD.
    Date:   25th November 2020
    Email:  nicholasehamilton@gmail.com
*/


const mongoose = require('mongoose');

class DatabaseConnection extends Object {
    
    constructor(host, user, password, database, useNewUrlParser = true, useUnifiedTopology = true, useFindAndModify=false, useCreateIndex = true){

        // Call super constructor;
        super();

        // Connection Status
        this._mongoose      = mongoose,     // For convenience
        this._connection    = undefined;
        this._connected     = false;

        // Credentials
        this._host          = host;
        this._user          = user;
        this._password      = password;
        this._database      = database;

        // Options
        this._options       = {
            useNewUrlParser,
            useUnifiedTopology,
            useFindAndModify,
            useCreateIndex
        }
        Object.keys(this._options).forEach(key => this._options[key] = Boolean(this._options[key]))

        // Bindings
        this.connect        = this.connect.bind(this);

        // Now default connect
        this.connect();
    }
    get mongoose(){
        return this._mongoose;
    }
    get connection(){
        return this._connection;
    }
    get connected(){
        return this._connected;
    }
    get user(){
        return this._user;
    }
    get password(){
        return this._password;
    }
    get host(){
        return this._host;
    }
    get database(){
        return this._database;
    }
    get options(){
        return this._options;
    }

    // Function to Connect
    connect(){

        // Get the connection string, raw and sanitized
        let {user, password, host, database} = this;
        const uri       = `mongodb+srv://${user}:${password}@${host}/${database}?retryWrites=true&w=majority`;
        const uriSan    = uri.replace(user, '<USER>').replace(password, '<PASSWORD>');

        // Try to connect
        try{

            const con = mongoose.createConnection(uri,this.options);
            
            con.on('error', () => {
                console.error(`Connection Error at URI ${uriSan}`);
            });
            con.once('open', () => {
                console.log(`Database Connected at URL ${uriSan}`);
                this._connected = true;
            });
            con.on('disconnected', () => {  
                console.log(`Database Disconnected at URI ${uriSan}`);
                this._connected  = false
            });
            
            this._connection = con;

        // Problem
        }catch(err){
        
            // Reporting
            console.error(err.message || `Problem Connecting at URI '${uriSan}'`);

            // Erase
            this._connected     = false;
            this._connection    = undefined;
        }
    }
}

module.exports = {
    mongoose,
    DatabaseConnection,
}