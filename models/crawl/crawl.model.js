/**
    ------------------------------------------------------------------------------
    
    ooooooooooooo ooooooooo.         .o.       ooooo oooooooooo.  oooooooooooo 
    8'   888   `8 `888   `Y88.      .888.      `888' `888'   `Y8b `888'     `8 
         888       888   .d88'     .8"888.      888   888     888  888         
         888       888ooo88P'     .8' `888.     888   888oooo888'  888oooo8    
         888       888`88b.      .88ooo8888.    888   888    `88b  888    "    
         888       888  `88b.   .8'     `888.   888   888    .88P  888       o 
        o888o     o888o  o888o o88o     o8888o o888o o888bood8P'  o888ooooood8    

    ------------------------------------------------------------------------------
    Crawl Model
    ------------------------------------------------------------------------------

    Author: Nicholas Hamilton, PhD.
    Date:   28th November 2020
    Email:  nicholasehamilton@gmail.com
*/

const {urlValidator}    = require('../../modules/utilities/validators/validators');

module.exports = function(mongoose, dbconn){

    const CrawlSchema = new mongoose.Schema({
        url             : { type: String,   required: true, index: {unique:true}, validate: urlValidator},
        domain          : { type: String,   required: true },
        statusCode      : { type: Number,   required: true, default: -1},
        RC2XX           : { type: Number,   required: true, default: 0},
        RC3XX           : { type: Number,   required: true, default: 0},
        RC4XX           : { type: Number,   required: true, default: 0},
        RC5XX           : { type: Number,   required: true, default: 0},
        failures        : { type: Number,   required: true, default: 0},
        processing      : { type: Boolean,  required: true, default: false},
        totalSuccesses  : { type: Number,   required: true, default: 0},
        totalFailures   : { type: Number,   required: true, default: 0},
        crawlDate       : { type: Date,     required: true, default: () => new Date()},
        crawlNext       : { type: Date,     required: true, default: () => new Date()}
    }, { timestamps : true });


    CrawlSchema.pre('validate', function(next) {
        if (!this.isModified('url')) 
            return next();
        
        const url = new URL(this.url);
        this.domain = url.hostname.replace('www.','');
        
        next();
    });

    // Replace missing fields if default is present
    CrawlSchema.pre('save', function(next) {
        Object.keys(this.schema.paths).forEach((key) => {
            let spk = this.schema.paths[key];
            if(spk.options.default && this[key] === null)
                this[key] = spk.options.default;
        });
        next();
    });

    let model;
    try {
        model = dbconn.model('Crawl');
    }catch(err){
        model = dbconn.model('Crawl', CrawlSchema);
    }
    return model;
}