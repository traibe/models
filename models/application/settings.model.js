/**
    ------------------------------------------------------------------------------
    
    ooooooooooooo ooooooooo.         .o.       ooooo oooooooooo.  oooooooooooo 
    8'   888   `8 `888   `Y88.      .888.      `888' `888'   `Y8b `888'     `8 
         888       888   .d88'     .8"888.      888   888     888  888         
         888       888ooo88P'     .8' `888.     888   888oooo888'  888oooo8    
         888       888`88b.      .88ooo8888.    888   888    `88b  888    "    
         888       888  `88b.   .8'     `888.   888   888    .88P  888       o 
        o888o     o888o  o888o o88o     o8888o o888o o888bood8P'  o888ooooood8    

    ------------------------------------------------------------------------------
    Settings Model
    ------------------------------------------------------------------------------

    Author: Nicholas Hamilton, PhD.
    Date:   28th November 2020
    Email:  nicholasehamilton@gmail.com
*/

const findOneOrCreate   = require('mongoose-findoneorcreate');
const {forceDefault}    = require('../../modules/utilities/functions');

module.exports = function(mongoose, dbconn){

    const {Mixed} = mongoose.Schema.Types;

    const SettingsSchema = new mongoose.Schema({
        maintenance:    { type: Boolean,    required: true, default: false},
        settings:       { type: Mixed,      required: true, default: {} }
    },{ timestamps  : true });

    SettingsSchema.plugin(findOneOrCreate);

    // Replace missing fields if default is present
    SettingsSchema.pre('save', forceDefault);

    let model;
    try {
        model = dbconn.model("Settings");
    }catch(err){
        model = dbconn.model("Settings",SettingsSchema);
    }
    return model;
}