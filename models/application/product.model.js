/**
    ------------------------------------------------------------------------------
    
    ooooooooooooo ooooooooo.         .o.       ooooo oooooooooo.  oooooooooooo 
    8'   888   `8 `888   `Y88.      .888.      `888' `888'   `Y8b `888'     `8 
         888       888   .d88'     .8"888.      888   888     888  888         
         888       888ooo88P'     .8' `888.     888   888oooo888'  888oooo8    
         888       888`88b.      .88ooo8888.    888   888    `88b  888    "    
         888       888  `88b.   .8'     `888.   888   888    .88P  888       o 
        o888o     o888o  o888o o88o     o8888o o888o o888bood8P'  o888ooooood8    

    ------------------------------------------------------------------------------
    Product Model
    ------------------------------------------------------------------------------

    Author: Nicholas Hamilton, PhD.
    Date:   28th November 2020
    Email:  nicholasehamilton@gmail.com
*/

module.exports = function(mongoose, dbconn){

    const unique = (x) => [...new Set(x)];

    const {Mixed} = mongoose.Schema.Types;

    const ProductSchema = new mongoose.Schema({
        key:        { type:String,  required: true, index: {unique:true} },
        name:       { type:String,  required: true },
        brand:      { type:String,  required: true },
        active:     { type:Boolean, required: true, default: false },
        attributes: { type:Mixed,   required: true, default:{} }
    }, { timestamps : true });

    /*
        Product Schema
    */
    ProductSchema.pre('save', function(next){
        const self = this;
        if (!self.isModified('attributes')) return next();
        let {region, country} = self.attributes;
        if (country){
            let locale = unique([(region === country || !Boolean(region)) ? country : `${country}, ${region}`, country]) // Unique only
            self.attributes = {...self.attributes, locale}
        }
        next()
    });

    let model;
    try{
        model = dbconn.model("Product");
    }catch(err){
        model = dbconn.model("Product", ProductSchema);
    }
    return model;
}