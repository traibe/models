/**
    ------------------------------------------------------------------------------
    
    ooooooooooooo ooooooooo.         .o.       ooooo oooooooooo.  oooooooooooo 
    8'   888   `8 `888   `Y88.      .888.      `888' `888'   `Y8b `888'     `8 
         888       888   .d88'     .8"888.      888   888     888  888         
         888       888ooo88P'     .8' `888.     888   888oooo888'  888oooo8    
         888       888`88b.      .88ooo8888.    888   888    `88b  888    "    
         888       888  `88b.   .8'     `888.   888   888    .88P  888       o 
        o888o     o888o  o888o o88o     o8888o o888o o888bood8P'  o888ooooood8    

    ------------------------------------------------------------------------------
    Product Listing Price model
    ------------------------------------------------------------------------------

    Author: Nicholas Hamilton, PhD.
    Date:   28th November 2020
    Email:  nicholasehamilton@gmail.com
*/

module.exports = function(mongoose, dbconn){

    /*
        Product Listing Price Schema
    */
    const ProductListingPriceSchema = new mongoose.Schema({
        _productListingId   : {type: mongoose.Types.ObjectId,   required: true},
        timestamp           : {type: Date,                      required: true},
        price               : {type: Number,                    required: true, min: 0.01}
    },{ timestamps : true });

    let model;
    try {
        model = dbconn.model("ProductListingPrice");
    }catch(err){
        model = dbconn.model("ProductListingPrice", ProductListingPriceSchema);
    }
    return model;
}