/**
    ------------------------------------------------------------------------------
    
    ooooooooooooo ooooooooo.         .o.       ooooo oooooooooo.  oooooooooooo 
    8'   888   `8 `888   `Y88.      .888.      `888' `888'   `Y8b `888'     `8 
         888       888   .d88'     .8"888.      888   888     888  888         
         888       888ooo88P'     .8' `888.     888   888oooo888'  888oooo8    
         888       888`88b.      .88ooo8888.    888   888    `88b  888    "    
         888       888  `88b.   .8'     `888.   888   888    .88P  888       o 
        o888o     o888o  o888o o88o     o8888o o888o o888bood8P'  o888ooooood8    

    ------------------------------------------------------------------------------
    User Model
    ------------------------------------------------------------------------------

    Author: Nicholas Hamilton, PhD.
    Date:   28th November 2020
    Email:  nicholasehamilton@gmail.com
*/

const bcrypt                        = require('bcryptjs');
const randToken                     = require('rand-token');
const SALT_WORK_FACTOR              = 12;
const USER_SECRET_LENGTH            = 16;
const {
    sendSystemEmail, 
    sendSystemSMS
}                                   = require('../../services');
const url                           = require('url');
const { forceDefault }              = require('../../modules/utilities/functions');
const locations                     = require('@traibe/common/locations/application/client');

const JWT_SECRET                    = process.env.JWT_SECRET;
const JWT_REFRESH_SECRET            = process.env.JWT_REFRESH_SECRET;
const COMPANY_URL                   = process.env.COMPANY_URL;
const COMPANY_NAME                  = process.env.COMPANY_NAME;

// Load the database
if(!JWT_SECRET|| !JWT_REFRESH_SECRET || !COMPANY_URL || !COMPANY_NAME)
    throw Error("JWT_SECRET, JWT_REFRESH_SECRET, COMPANY_URL and COMPANY_NAME are required environmental variables");


module.exports = function(mongoose, dbconn){

    const UserNotifications = require('./userNotifications.model')(mongoose, dbconn);
    const Token             = require('./token.model')(mongoose, dbconn);
    const Role              = require('./role.model')(mongoose, dbconn);

    const UserSchema = new mongoose.Schema({
        email:          {type:String,   required: true, index: {unique:true} },
        handle:         {type:String,   required: true },
        firstname:      {type:String,   required: true, default: 'Firstname' },
        lastname:       {type:String,   required: true, default: 'Lastname' },
        emailConfirmed: {type:Boolean,  default:  false },
        password:       {type:String,   required: true  },
        masterSecret:   {type:String,   required: false },
        userSecret:     {type:String,   required: false },
        suspended:      {type:Boolean,  required: true, default: false },
        deleted:        {type:Boolean,  required: true, default: false },
        lastSeen:       {type:Date,     required: false},
        roles:          [{
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'Role'
        }]
    },{ timestamps : true });

    // Replace missing fields if default is present
    UserSchema.pre('save', forceDefault);

    UserSchema.pre('save', async function(next){
        try{
            if(this._id){
                await UserNotifications.findOneOrCreate(
                    {_userId : this._id},
                    {_userId : this._id}
                );
            }
        }catch(err){
            console.error(err.message);
        }
        next();
    })

    // Hash Password
    UserSchema.pre('save', function(next) {
        const self = this;
        // only hash the password if it has been modified (or is new)
        if (!self.isModified('password')) return next();
        // generate a salt
        bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
            if (err) return next(err);
            // hash the password using our new salt
            bcrypt.hash(self.password, salt, function(err, hash) {
                if (err) return next(err);
                // override the cleartext password with the hashed one
                self.password = hash;
                next();
            });
        });
    });

    UserSchema.pre('save', async function(next){
        const self = this;
        if(!self.isModified('password')) return next();
        // Now send email notice that the password has changed.
        let subjectPrefix = self.firstname ? `${self.firstname}, ` : ``;
        try{
            sendSystemEmail(
                to                  =   self.email,
                subject             =   `${subjectPrefix}Your password has been changed`,
                body                =   "Your password has been changed, if this was not you, please reset your password via the link below",
                name                =   self.getNameForEmail(),
                link_button         =   url.resolve(COMPANY_URL,locations.userPasswordResetRequest),
                link_button_text    =   "Reset Password"
            );
        }catch(err){
            console.log(err);
        }
        next();
    })

    UserSchema.pre('save', async function(next){
        const self = this;
        if(!self.isModified('suspended')) 
            return next();
        self.notifySuspension();
        next();
    })

    UserSchema.pre('validate', function(next){
        const self = this;
        if(!self.handle)
            self.handle = self._id;
        next();
    })

    // Compare two passwords
    UserSchema.methods.comparePassword = async function(candidatePassword) {
        return new Promise((resolve, reject) => {
            bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
                if (err) return reject(err);
                resolve(isMatch);
            });
        })
    };

    // Check the role exists
    UserSchema.methods.hasRole = async function(role){
        try {
            const docs  = await Role.find({_id:{$in:this.roles}});
            const roles = docs.map( item => item.name )
            return roles.includes(role);
        }catch(err){
            return false;
        }
    }

    // Touch the user
    UserSchema.methods.touch = async function(){
        this.lastSeen = Date.now();
        return await this.save();
    }

    /*
        Log out all devices for User
        ---------------------------------------------------
        Logs out all devices for a given user, by generating
        a fresh userSecret.
    */
    UserSchema.methods.logOutUser = async function(){
        this.userSecret = randToken.generate(USER_SECRET_LENGTH)
        return await this.save() //promise
    }

    /*
        Log out all devices for all users
        ---------------------------------------------------
        Logs out all devices for a given user, by generating
        a fresh userSecret.
    */
    UserSchema.statics.logOutAll = async function(){
        let masterSecret = randToken.generate(USER_SECRET_LENGTH);
        return await this.updateMany({},{$set:{masterSecret}});
    }

    /*
        Composite secret for JWT Encoding:
        ---------------------------------------------------

        We make a composite secret from 3 sources:

        1. Applicaiton secret
        2. User Password hash
        3. User secret

        Changing #1 will act as a kind of kill-switch forcing ALL users tokens to be invalid
        Changing #2 will cause all devices to be logged out for a user when they change passwords
        Changing #3 will cause all devices to be logged out, and is intended to be issued randomly
                    when the user interacts with the UI and might say click a button saying log-out
                    all devices
    */
    UserSchema.methods.jwtCompositeSecret = async function(cb){
        let self = this;
        return new Promise(async (resolve, reject) =>{
            const fun = (refresh = false) => {    
                let {password, masterSecret, userSecret} = self;
                let jwtSecret = refresh ? JWT_REFRESH_SECRET : JWT_SECRET;
                return [jwtSecret, masterSecret, userSecret, password].filter(Boolean).join('.'); 
            } 

            try{
                if(!self.userSecret)
                    self = await self.logOutUser(); // New userSecret issued.
                return resolve({
                    jwt_secret:         fun(false),
                    jwt_refresh_secret: fun(true),
                })
            }catch(err){
                return reject(err)
            }
        })
    }

    UserSchema.methods.getNameForEmail = function(){
        return `${this.firstname || ''} ${this.lastname || ''}`.trim() || this.handle || '';
    }

    UserSchema.methods.issueToken = async function(){
        const self = this;
        return new Promise(async (resolve, reject)=>{
            new Token({_userId:self._id}).save((err,obj)=>{
                if(err) return reject(err);
                return resolve(obj.token);
            });
        })
    }
    UserSchema.methods.sendPasswordResetEmail = async function(){
        const self = this;
        return new Promise(async (resolve, reject)=>{
            let token, to, subjectPrefix, subject, body, name, link, link_text;
            token                   = await self.issueToken();
            to                      = self.email
            subjectPrefix           = self.firstname ? `${self.firstname}, ` : ``;
            subject                 = `${subjectPrefix}Your password reset link is enclosed`;
            body                    = "Recently you requested for your password to be reset, click the button below to reset your password";
            name                    = await self.getNameForEmail();
            link_button             = url.resolve(COMPANY_URL,locations.userPasswordResetToken).replace(':token',token)
            link_button_text        = 'Reset Password'
            let {success, message}  = await sendSystemEmail(to=to,subject=subject,body=body,name=name,link_button=link_button,link_button_text=link_button_text);
            if(success) return resolve({success,message})
            else return reject({success, message})
        })
    }
    UserSchema.methods.sendConfirmationEmail = async function(){
        const self = this;
        return new Promise(async (resolve, reject)=>{
            if(self.emailConfirmed) 
                return reject({success:false, message: "Users email is already confirmed"});
            let token, to, subjectPrefix, subject, body, name, link, link_text;
            token                   = await self.issueToken();
            to                      = self.email
            subjectPrefix           = self.firstname ? `${self.firstname}, ` : ``;
            subject                 = `${subjectPrefix}Please confirm your email address`
            body                    = `Recently you joined ${COMPANY_NAME}, please click the button below to confirm your account`
            name                    = self.getNameForEmail()
            link_button             = url.resolve(COMPANY_URL,locations.userRegisterConfirmToken).replace(':token',token)
            link_button_text        = 'Confirm Email'
            let {success, message}  = await sendSystemEmail(to=to,subject=subject,body=body,name=name,link_button=link_button,link_button_text=link_button_text);
            if(success) return resolve({success,message})
            else return reject({success, message})
        })
    }

    UserSchema.methods.notifySuspension = async function(){
        const self = this;
        if(self.suspended){
            try{
                let subjectPrefix = self.firstname ? `${self.firstname}, ` : ``;
                sendSystemEmail(
                    to          =   self.email,
                    subject     =   `${subjectPrefix}Your account is suspended`,
                    body        =   "This is a notice to inform you that your account has been suspended. Appologies for the inconvenience",
                    name        =   self.getNameForEmail()
                );
            }catch(err){
                console.log(err);
            }
        }
    }

    UserSchema.methods.notifyLogin = async function(req){
        const self = this;
        // Now send email notice that the password has changed.
        let subjectPrefix = self.firstname ? `${self.firstname}, ` : ``;
        try{
            let address = '', agent = '';
            if(req && req.headers){
                //IP Address
                address = req.headers['x-forwarded-for'] || req.connection.remoteAddress; 
                address = address ? ` from IP address: ${address}` : ''
                //User Agent
                agent   = req.headers['user-agent']; 
                agent   = agent ? ` (${agent})` : ''
            }
            sendSystemEmail(
                to                  =   self.email,
                subject             =   `${subjectPrefix}Your account has been logged in`,
                body                =   `Someone has successfully logged in using your credentials${address}${agent}. If this was not you, please reset your password via the link below.`,
                name                =   self.getNameForEmail(),
                link_button         =   url.resolve(COMPANY_URL,locations.userPasswordResetRequest),
                link_button_text    =   "Reset Password"
            );
        }catch(err){
            console.log(err);
        }
    }

    UserSchema.methods.hasRole = async function(roleName){
        try{
            const roles = await Role.find({_id:{$in:this.roles}});
            if(roles.length === 0) 
                return false;
            const nMatch = roles
                .map(item => item.name)
                .filter(item => item.toLowerCase() === roleName.toString().toLowerCase())
                .length;
            return Boolean(nMatch > 0)
        }catch(err){
            console.log(err)
        }
        return false; //Default
    }

    let model;
    try {
        model = dbconn.model("User");
    }catch(err){
        model = dbconn.model("User",UserSchema);
    }
    return model;
}
