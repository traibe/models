/**
    ------------------------------------------------------------------------------
    
    ooooooooooooo ooooooooo.         .o.       ooooo oooooooooo.  oooooooooooo 
    8'   888   `8 `888   `Y88.      .888.      `888' `888'   `Y8b `888'     `8 
         888       888   .d88'     .8"888.      888   888     888  888         
         888       888ooo88P'     .8' `888.     888   888oooo888'  888oooo8    
         888       888`88b.      .88ooo8888.    888   888    `88b  888    "    
         888       888  `88b.   .8'     `888.   888   888    .88P  888       o 
        o888o     o888o  o888o o88o     o8888o o888o o888bood8P'  o888ooooood8    

    ------------------------------------------------------------------------------
    Token Model
    ------------------------------------------------------------------------------

    Author: Nicholas Hamilton, PhD.
    Date:   28th November 2020
    Email:  nicholasehamilton@gmail.com
*/

const crypto                        = require('crypto');
const EXPIRY_SECONDS                = 15 * 60; // 15 minutes

module.exports = function(mongoose, dbconn){

    const TokenSchema = new mongoose.Schema({
        _userId:        { type: mongoose.Schema.Types.ObjectId, required:true, ref: 'User'},
        token:          { type:String, required: true},
        createdAt:      { type: Date, required: true, default: Date.now, expires: EXPIRY_SECONDS }
    });

    TokenSchema.pre('validate', function(next) {
        const self = this;
        if(!self.token)
            self.token = crypto.randomBytes(16).toString('hex')
        return next();
    })

    let model;
    try {
        model = dbconn.model("Token");
    }catch(err){
        model = dbconn.model("Token",TokenSchema);
    }
    return model;
}