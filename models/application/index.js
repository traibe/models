

/**
    ------------------------------------------------------------------------------
    
    ooooooooooooo ooooooooo.         .o.       ooooo oooooooooo.  oooooooooooo 
    8'   888   `8 `888   `Y88.      .888.      `888' `888'   `Y8b `888'     `8 
         888       888   .d88'     .8"888.      888   888     888  888         
         888       888ooo88P'     .8' `888.     888   888oooo888'  888oooo8    
         888       888`88b.      .88ooo8888.    888   888    `88b  888    "    
         888       888  `88b.   .8'     `888.   888   888    .88P  888       o 
        o888o     o888o  o888o o88o     o8888o o888o o888bood8P'  o888ooooood8    

    ------------------------------------------------------------------------------
    Application Models
    ------------------------------------------------------------------------------
    Permits multi connections, via this method:
    https://stackoverflow.com/a/15957914/1834057

    Author: Nicholas Hamilton, PhD.
    Date:   28th November 2020
    Email:  nicholasehamilton@gmail.com
*/

module.exports = function (mongoose, dbconn){
    return {
        Contact                   : require('./contact.model')(mongoose, dbconn),
        Product                   : require('./product.model')(mongoose, dbconn),
        ProductListing            : require('./productListing.model')(mongoose, dbconn),
        ProductListingPrice       : require('./productListingPrice.model')(mongoose, dbconn),
        ProductListingUnassigned  : require('./productListingUnassigned.model')(mongoose, dbconn),
        ProductChoice             : require('./productChoice.model')(mongoose, dbconn),
        Role                      : require('./role.model')(mongoose, dbconn),
        Settings                  : require('./settings.model')(mongoose, dbconn),
        Token                     : require('./token.model')(mongoose, dbconn),
        User                      : require('./user.model')(mongoose, dbconn),
        UserNotifications         : require('./userNotifications.model')(mongoose, dbconn),
    }
}