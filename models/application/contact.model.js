/**
    ------------------------------------------------------------------------------
    
    ooooooooooooo ooooooooo.         .o.       ooooo oooooooooo.  oooooooooooo 
    8'   888   `8 `888   `Y88.      .888.      `888' `888'   `Y8b `888'     `8 
         888       888   .d88'     .8"888.      888   888     888  888         
         888       888ooo88P'     .8' `888.     888   888oooo888'  888oooo8    
         888       888`88b.      .88ooo8888.    888   888    `88b  888    "    
         888       888  `88b.   .8'     `888.   888   888    .88P  888       o 
        o888o     o888o  o888o o88o     o8888o o888o o888bood8P'  o888ooooood8    

    ------------------------------------------------------------------------------
    Contact Model
    ------------------------------------------------------------------------------

    Author: Nicholas Hamilton, PhD.
    Date:   28th November 2020
    Email:  nicholasehamilton@gmail.com
*/

const {sendSystemEmail} = require('../../services');

module.exports = function(mongoose, dbconn){

    const ContactSchema = new mongoose.Schema({
        firstname   : {type : String, required: true},
        lastname    : {type : String, required: true},
        phone       : {type : String},
        email       : {type : String, required: true},
        subject     : {type : String, required: true},
        message     : {type : String, required: true}
    }, {timestamps : true});

    ContactSchema.methods.sendConfirmationEmail = function(){
        let {firstname, lastname, email} = this;
        return new Promise(async (resolve, reject) => {
            try{
                let name    = [firstname,lastname].filter(Boolean).join(' ').trim() || ''
                let {success, message} = await sendSystemEmail(
                    to      = email,
                    subject = `${firstname ? `${firstname}, ` : ``}Contact Received`,
                    body    = "We have receied your query and will endevour to get back to you shortly.",
                    name    = name
                );
                if(!success) 
                    throw new Exception(message || "Problem sending email");
                return resolve({success, message})
            }catch(err){
                return reject({success: false, message: err.message});
            }
        });
    }

    let model;
    try {
        model = dbconn.model("Contact");
    }catch(err){
        model = dbconn.model("Contact", ContactSchema);
    }
    return model;
}