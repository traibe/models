/**
    ------------------------------------------------------------------------------
    
    ooooooooooooo ooooooooo.         .o.       ooooo oooooooooo.  oooooooooooo 
    8'   888   `8 `888   `Y88.      .888.      `888' `888'   `Y8b `888'     `8 
         888       888   .d88'     .8"888.      888   888     888  888         
         888       888ooo88P'     .8' `888.     888   888oooo888'  888oooo8    
         888       888`88b.      .88ooo8888.    888   888    `88b  888    "    
         888       888  `88b.   .8'     `888.   888   888    .88P  888       o 
        o888o     o888o  o888o o88o     o8888o o888o o888bood8P'  o888ooooood8    

    ------------------------------------------------------------------------------
    Product Listing Model
    ------------------------------------------------------------------------------

    Author: Nicholas Hamilton, PhD.
    Date:   28th November 2020
    Email:  nicholasehamilton@gmail.com
*/

const {urlValidator} = require('../../modules/utilities/validators/validators');

module.exports = function(mongoose, dbconn){

    /*
        Product Listing Schema
    */
    const ProductListingSchema = new mongoose.Schema({
        _productId  : { type: mongoose.Types.ObjectId,  required: true},
        url         : { type: String, required: true, validate: urlValidator },
        description : { type: String, default: ""},
        active      : { type: Boolean,required: true, default: false },
        region      : { type: String},
        crawling    : { type: Date}
    },{ timestamps: true });

    // Unique URL per _productID
    ProductListingSchema.index({ _productId: 1, url: 1}, { unique: true }); 

    let model;
    try {
        model = dbconn.model("ProductListing");
    }catch(err){
        model = dbconn.model("ProductListing", ProductListingSchema);
    }
    return model;
}