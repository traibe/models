/**
    ------------------------------------------------------------------------------
    
    ooooooooooooo ooooooooo.         .o.       ooooo oooooooooo.  oooooooooooo 
    8'   888   `8 `888   `Y88.      .888.      `888' `888'   `Y8b `888'     `8 
         888       888   .d88'     .8"888.      888   888     888  888         
         888       888ooo88P'     .8' `888.     888   888oooo888'  888oooo8    
         888       888`88b.      .88ooo8888.    888   888    `88b  888    "    
         888       888  `88b.   .8'     `888.   888   888    .88P  888       o 
        o888o     o888o  o888o o88o     o8888o o888o o888bood8P'  o888ooooood8    

    ------------------------------------------------------------------------------
    User Notifications Model
    ------------------------------------------------------------------------------

    Author: Nicholas Hamilton, PhD.
    Date:   28th November 2020
    Email:  nicholasehamilton@gmail.com
*/

const findOneOrCreate               = require('mongoose-findoneorcreate');
const {forceDefault}                = require('../../modules/utilities/functions');

module.exports = function(mongoose, dbconn){

    const UserNotificationsSchema = new mongoose.Schema({
        _userId         : {type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User', index: {unique:true}},
        login           : {type: Boolean,  required: true, default: true  },
        passwordChange  : {type: Boolean,  required: true, default: true  },
        newsletter      : {type: Boolean,  required: true, default: false },
        dealsDaily      : {type: Boolean,  required: true, default: false },
        dealsWeekly     : {type: Boolean,  required: true, default: false }
    },
    { timestamps : true });

    // FindOneOrCreate model
    UserNotificationsSchema.plugin(findOneOrCreate);

    // Replace missing fields if default is present
    UserNotificationsSchema.pre('save', forceDefault);

    let model;
    try {
        model = dbconn.model('UserNotifications');
    }catch(err){
        model = dbconn.model('UserNotifications',UserNotificationsSchema);
    }
    return model;
}