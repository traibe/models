/**
    ------------------------------------------------------------------------------
    
    ooooooooooooo ooooooooo.         .o.       ooooo oooooooooo.  oooooooooooo 
    8'   888   `8 `888   `Y88.      .888.      `888' `888'   `Y8b `888'     `8 
         888       888   .d88'     .8"888.      888   888     888  888         
         888       888ooo88P'     .8' `888.     888   888oooo888'  888oooo8    
         888       888`88b.      .88ooo8888.    888   888    `88b  888    "    
         888       888  `88b.   .8'     `888.   888   888    .88P  888       o 
        o888o     o888o  o888o o88o     o8888o o888o o888bood8P'  o888ooooood8    

    ------------------------------------------------------------------------------
    Product Choice Model
    ------------------------------------------------------------------------------

    Author: Nicholas Hamilton, PhD.
    Date:   28th November 2020
    Email:  nicholasehamilton@gmail.com
*/

module.exports = function(mongoose, dbconn){

    /*
        Product Choice Schema
    */
    const ProductChoiceSchema = new mongoose.Schema({
        _productId  : { type: mongoose.Types.ObjectId,  required: true, index: {unique:true} },
        choice      : [
            {
                _productId  : { type: mongoose.Types.ObjectId,  required: true },
                k           : { type: Number, required: true},
                score       : { type: Number, required: true},
                isRef       : { type: Boolean,required: true, default: false}
            }
        ]
    },{ timestamps : true });

    // Autogenerate isRef field
    ProductChoiceSchema.pre('save',async function(next){
        const self = this;
        if(!self.isModified('choice')) return next();
        self.choice = self.choice.map((item)=>{
            item.isRef = String(item._productId) === String(self._productId);
            return item;
        })
        next();
    });

    let model;
    try {
        model = dbconn.model("ProductChoice");
    }catch(err){
        model = dbconn.model("ProductChoice",ProductChoiceSchema);
    }
    return model;
}