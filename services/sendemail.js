/**
    ------------------------------------------------------------------------------
    
    ooooooooooooo ooooooooo.         .o.       ooooo oooooooooo.  oooooooooooo 
    8'   888   `8 `888   `Y88.      .888.      `888' `888'   `Y8b `888'     `8 
         888       888   .d88'     .8"888.      888   888     888  888         
         888       888ooo88P'     .8' `888.     888   888oooo888'  888oooo8    
         888       888`88b.      .88ooo8888.    888   888    `88b  888    "    
         888       888  `88b.   .8'     `888.   888   888    .88P  888       o 
        o888o     o888o  o888o o88o     o8888o o888o o888bood8P'  o888ooooood8    

    ------------------------------------------------------------------------------
    Send System Email
    ------------------------------------------------------------------------------

    Author: Nicholas Hamilton, PhD.
    Date:   28th November 2020
    Email:  nicholasehamilton@gmail.com
*/

const url               = require('url');
const {publishToTopic}  = require('@traibe/sns');
const locations         = require('@traibe/common/locations/application/client');

const SEND_EMAIL_TOPIC  = 'ms-email-send';
const COMPANY_URL       = process.env.COMPANY_URL;
const link_unsubscribe  = url.resolve(COMPANY_URL,  locations.userProfileNotifications);
const logo              = url.resolve(COMPANY_URL,  'logo.png');

const sendSystemEmail = async (to, subject, body, name, link_button = undefined, link_button_text = undefined) => {
    try{
        const send      = await publishToTopic(SEND_EMAIL_TOPIC);
        const result    = await send({ to, subject, body, name, link_button, link_button_text, link_unsubscribe, logo});
        return {success: true, message: 'Email sent', result};
    }catch(err){
        console.error(err);
        return {success: false, message: `Email failed: ${err.message}`}
    }
}

module.exports = { sendSystemEmail };