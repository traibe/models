/**
    ------------------------------------------------------------------------------
    
    ooooooooooooo ooooooooo.         .o.       ooooo oooooooooo.  oooooooooooo 
    8'   888   `8 `888   `Y88.      .888.      `888' `888'   `Y8b `888'     `8 
         888       888   .d88'     .8"888.      888   888     888  888         
         888       888ooo88P'     .8' `888.     888   888oooo888'  888oooo8    
         888       888`88b.      .88ooo8888.    888   888    `88b  888    "    
         888       888  `88b.   .8'     `888.   888   888    .88P  888       o 
        o888o     o888o  o888o o88o     o8888o o888o o888bood8P'  o888ooooood8    

    ------------------------------------------------------------------------------
    Send System SMS
    ------------------------------------------------------------------------------

    Author: Nicholas Hamilton, PhD.
    Date:   28th November 2020
    Email:  nicholasehamilton@gmail.com
*/

const {publishToTopic}  = require('@traibe/sns');

// Constants
const SEND_SMS_TOPIC    = 'ms-sms-send';
const SEND_SMS_SENDER   = process.env.COMPANY_NAME || 'Notice';

const sendSystemSMS = async (phoneNumber, message, senderID = SEND_SMS_SENDER) => {
    try{
        const send    = await publishToTopic(SEND_SMS_TOPIC);
        const result  = await send({phoneNumber, message, senderID});
        return {success:true, message: "SMS sent", result}
    }catch(err){
        console.error(err);
        return {success:false, message: `SMS failed: ${err}`}
    }
}

module.exports = {
    sendSystemSMS
};